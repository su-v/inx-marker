#!/usr/bin/env python
"""
Copyright (C) 2006 Aaron Spike, aaron@ekips.org
Copyright (C) 2007 Terry Brown, terry_n_brown@yahoo.com
Copyright (C) 2010 Nicolas Dufour, nicoduf@yahoo.fr (color options)
Copyright (C) 2014-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=missing-docstring

# standard library
import copy
import sys

# local library
import inkex
import simplestyle
import simpletransform


try:
    inkex.localize()
except AttributeError:
    import gettext  # pylint: disable=wrong-import-order
    _ = gettext.gettext


DEBUG = 0
MARKER_WRAP = 'marker_wrap'


def invertTransform(mat):
    """Return inverted mat."""
    # pylint: disable=invalid-name
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    if det != 0:  # det is 0 only in case of 0 scaling
        # invert the rotation/scaling part
        a11 = mat[1][1]/det
        a12 = -mat[0][1]/det
        a21 = -mat[1][0]/det
        a22 = mat[0][0]/det
        # invert the translational part
        a13 = -(a11*mat[0][2] + a12*mat[1][2])
        a23 = -(a21*mat[0][2] + a22*mat[1][2])
        return [[a11, a12, a13], [a21, a22, a23]]
    else:
        return [[0, 0, -mat[0][2]], [0, 0, -mat[1][2]]]


def mat_invert(mat):
    """Return inverted transformation matrix."""
    if hasattr(simpletransform, 'invertTransform'):
        return simpletransform.invertTransform(mat)
    else:
        return invertTransform(mat)


def run(command_format, prog_name, stdin_str=None, verbose=False):
    """run command"""
    # pylint: disable=invalid-name
    # pylint: disable=broad-except
    # pylint: disable=unused-variable
    # pylint: disable=too-many-branches
    if verbose:
        inkex.debug(command_format)
    msg = None
    try:
        try:
            from subprocess import Popen, PIPE
            if isinstance(command_format, list):
                p = Popen(command_format, shell=False,
                          stdin=PIPE, stdout=PIPE, stderr=PIPE)
                # TODO python3: verify converting *stdin_str* to bytes
                if sys.version_info > (2,):
                    if stdin_str is not None:
                        stdin_str = stdin_str.encode()
                out, err = p.communicate(stdin_str)
            elif isinstance(command_format, str):
                p = Popen(command_format, shell=True,
                          stdin=PIPE, stdout=PIPE, stderr=PIPE)
                # TODO python3: verify converting *stdin_str* to bytes
                if sys.version_info > (2,):
                    if stdin_str is not None:
                        stdin_str = stdin_str.encode()
                out, err = p.communicate(stdin_str)
            else:
                msg = "unsupported command format %s" % type(command_format)
        except ImportError:
            msg = "subprocess.Popen not available"
        # if err and msg is None:
        #     msg = "%s failed:\n%s\n%s\n" % (prog_name, out, err)
    except Exception as inst:
        msg = "Error attempting to run %s: %s" % (prog_name, str(inst))
    if msg is None:
        # inkex.debug(type(out))
        if sys.version_info < (3,):
            return out
        else:
            # TODO python3: verify converting *out* to string (unicode)
            return out.decode()
    else:
        inkex.errormsg(msg)
        sys.exit(1)


def query_bbox(svg_file, obj_id):
    '''
    parameters: svg file, id
    queries inkscape on the command line for x, y, width, height of
        object with id
    returns: list with x, y, width, height
    '''
    opts = ['inkscape', '--shell']
    stdin_str = ""
    for arg in ['x', 'y', 'width', 'height']:
        stdin_str += '--file={0} --query-id={1} --query-{2}\n'.format(
            svg_file, obj_id, arg)
    stdout_str = run(opts, 'inkscape', stdin_str, verbose=False).split('>')
    if len(stdout_str) >= 5:
        return stdout_str[1:5]


def get_style(node):
    try:
        style = simplestyle.parseStyle(node.get('style'))
    except AttributeError:
        inkex.errormsg(_(
            "No style attribute found for id: %s") % node.get('id'))
        style = None
    return style


def set_marker_style(node, markerstyle):
    cstyle = simplestyle.parseStyle(node.get('style'))
    # TODO: we have 'Keep' as option - is this sufficient to preserve style?
    # marker fill
    if markerstyle['fill'] is not None:
        cstyle['fill'] = markerstyle['fill']
    # marker stroke
    if markerstyle['stroke'] is not None:
        cstyle['stroke'] = markerstyle['stroke']
    # marker opacity
    for prop in ('fill-opacity', 'stroke-opacity'):
        if markerstyle[prop] is not None:
            cstyle[prop] = markerstyle[prop]
    node.set('style', simplestyle.formatStyle(cstyle))


class CustomMarkers(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option(
            "--marker_fill", action="store", type="string",
            dest="marker_fill", default="stroke",
            help="Match markers' fill with object color")
        self.OptionParser.add_option(
            "--marker_stroke", action="store", type="string",
            dest="marker_stroke", default="stroke",
            help="Match markers' stroke with object color")
        self.OptionParser.add_option(
            "-c", "--context",
            action="store", type="inkbool",
            dest="context", default=False,
            help="Use context paint (SVG2)")
        self.OptionParser.add_option(
            "-i", "--invert",
            action="store", type="inkbool",
            dest="invert", default=False,
            help="Invert fill and stroke colors")
        self.OptionParser.add_option(
            "-a", "--alpha",
            action="store", type="inkbool",
            dest="assign_alpha", default=True,
            help="Assign the object fill and stroke alpha to the markers")
        self.OptionParser.add_option(
            "--fill_type",
            action="store", type="string",
            dest="fill_type", default="assign",
            help="Match markers' fill with custom color")
        self.OptionParser.add_option(
            "--fill_color",
            action="store", type="int",
            dest="fill_color", default=1364325887,
            help="Choose a custom fill color")
        self.OptionParser.add_option(
            "--stroke_type",
            action="store", type="string",
            dest="stroke_type", default="assign",
            help="Match markers' stroke with custom color")
        self.OptionParser.add_option(
            "--stroke_color",
            action="store", type="int",
            dest="stroke_color", default=1364325887,
            help="Choose a custom stroke color")
        self.OptionParser.add_option(
            "-g", "--getput",
            action="store", type="string",
            dest="getput", default="Apply",
            help="Apply or Edit marker from path")
        self.OptionParser.add_option(
            "-m", "--mid",
            action="store", type="inkbool",
            dest="marker-mid", default=True,
            help="Act on mid-points")
        self.OptionParser.add_option(
            "-s", "--start",
            action="store", type="inkbool",
            dest="marker-start", default=True,
            help="Act on start-points")
        self.OptionParser.add_option(
            "-e", "--end",
            action="store", type="inkbool",
            dest="marker-end", default=True,
            help="Act on end-points")
        self.OptionParser.add_option(
            "--all",
            action="store", type="inkbool",
            dest="marker", default=False,
            help="Act on all points ('marker' shorthand)")
        self.OptionParser.add_option(
            "-o", "--orient",
            action="store", type="string",
            dest="orient", default="auto",
            help="Marker orientation")
        self.OptionParser.add_option(
            "--angle",
            action="store", type="float",
            dest="angle", default=0.0,
            help="Fixed angle for marker orientation")
        self.OptionParser.add_option(
            "--scale",
            action="store", type="inkbool",
            dest="scale", default=True,
            help="Scale with stroke width")
        self.OptionParser.add_option(
            "--css_px",
            action="store", type="inkbool",
            dest="css_px", default=False,
            help="Scale marker object to CSS Pixel")
        self.OptionParser.add_option(
            "--modify_for_object",
            action="store", type="inkbool",
            dest="modify_for_object", default=False,
            help="Do not create a copy, modify the markers")
        self.OptionParser.add_option(
            "--modify_for_custom",
            action="store", type="inkbool",
            dest="modify_for_custom", default=False,
            help="Do not create a copy, modify the markers")
        self.OptionParser.add_option(
            "--debug",
            action="store", type="inkbool",
            dest="debug", default=False,
            help="Show debug messages")
        self.OptionParser.add_option(
            "--tab",
            action="store", type="string",
            dest="tab",
            help="The selected UI-tab when OK was pressed")
        self.OptionParser.add_option(
            "--colortab",
            action="store", type="string",
            dest="colortab",
            help="The selected cutom color tab when OK was pressed")
        self.OptionParser.add_option(
            "--apply_edit_tab",
            action="store", type="string",
            dest="apply_edit_tab",
            help="The selected mode for Apply/Edit")
        self.msg = {
            'Apply': "The 'Apply' mode requires a path " +
                     "and a marker as selection.",
            'Edit': "The 'Edit' mode requires a single path " +
                    "with applied marker(s) as selection.",
            'Positions': "Nothing to do (no marker positions specified.)",
        }

    # --- Helper methods ---

    def get_defs(self, defs=None):
        defs = self.xpathSingle('/svg:svg//svg:defs')
        if defs is None:
            defs = inkex.etree.Element(inkex.addNS('defs', 'svg'))
            self.document.getroot().append(defs)
        return defs

    def get_options(self, opts, olist=None):
        if olist is None:
            olist = []
        for i in opts:
            if getattr(self.options, i):
                olist.append(i)
        return olist

    def wrap_group(self, node, string):
        node_wrap = inkex.etree.Element(inkex.addNS('g', 'svg'))
        node_wrap.set('id', self.uniqueId(string))
        node_wrap.append(node)
        return node_wrap

    # --- Edit and Apply ---

    def edit_markers(self, path, defs, positions):
        # pylint: disable=unused-argument
        style = get_style(path)
        scale = self.unittouu('1px')
        if len(style):
            for mprop in positions:
                if mprop not in style:
                    continue
                target_id = style[mprop][5:-1]
                target = self.getElementById(target_id)
                for i in target.iterchildren():
                    if i.get('id').startswith(MARKER_WRAP):
                        self.current_layer.append(copy.deepcopy(i[0]))
                    else:
                        self.current_layer.append(copy.deepcopy(i))
                    if self.options.css_px:
                        node = self.current_layer[-1]
                        mat = simpletransform.parseTransform(
                            'scale({0})'.format(scale))
                        simpletransform.applyTransformToNode(mat, node)
                del style[mprop]
            path.set('style', simplestyle.formatStyle(style))
        else:
            inkex.errormsg(self.msg['Edit'])

    def get_marker_bbox(self, marker):
        # pylint: disable=too-many-locals
        vis_bbox = query_bbox(self.svg_file, marker.get('id'))
        if len(vis_bbox) == 4:
            scale = self.unittouu('1px')
            if self.options.css_px:
                # Scale marker (in SVG user units) to CSS Pixels
                # Keeps marker size constant for stroke width = 1 CSS pixel
                mat = simpletransform.parseTransform(
                    'scale({0})'.format(1/scale))
                simpletransform.applyTransformToNode(mat, marker)
                x, y, width, height = [float(i) for i in vis_bbox]
            else:
                # Scale query result (in CSS px) to SVG user units
                # Keeps marker size constant for stroke width = 1 SVG user unit
                x, y, width, height = [float(i)*scale for i in vis_bbox]
            # Calculate reference center coordinates for marker definition
            bbox_center = [x + width/2, y + height/2]
            # Compensate offsets of parent containers
            ident_mat = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]
            mat = simpletransform.composeParents(marker.getparent(), ident_mat)
            invert_mat = mat_invert(mat)
            simpletransform.applyTransformToPoint(invert_mat, bbox_center)
            # Wrap group around preserved transforms of marker
            if marker.get('transform') is not None:
                marker = self.wrap_group(marker, MARKER_WRAP)
        else:
            inkex.errormsg("Inkscape query for visual bbox failed, " +
                           "falling back to geom bbox.")
            if self.options.css_px:
                # Scale marker (in SVG user units) to CSS Pixels
                mat = simpletransform.parseTransform(
                    'scale({0})'.format(1/scale))
                simpletransform.applyTransformToNode(mat, marker)
            # Wrap group around preserved transforms of marker
            if marker.get('transform') is not None:
                marker = self.wrap_group(marker, MARKER_WRAP)
            xmin, xmax, ymin, ymax = simpletransform.computeBBox([marker])
            bbox_center = [xmin + (xmax-xmin)/2.0, ymin + (ymax-ymin)/2.0]
            stroke_factor = 1.2
            width = (xmax - xmin) * stroke_factor
            height = (ymax - ymin) * stroke_factor
        return marker, [width, height], bbox_center

    def apply_marker(self, marker, path, defs, positions):
        # get marker size & ref
        marker, bbox, ref_point = self.get_marker_bbox(marker)
        # create marker definition in defs
        marker_def = inkex.etree.SubElement(defs, inkex.addNS('marker', 'svg'))
        marker_def_id = self.uniqueId('marker')
        marker_def.set('id', marker_def_id)
        marker_def.set('markerWidth', str(bbox[0]))
        marker_def.set('markerHeight', str(bbox[1]))
        marker_def.set('refX', str(ref_point[0]))
        marker_def.set('refY', str(ref_point[1]))
        # TODO: do we still need to set overflow to visible?
        marker_def.set('overflow', 'visible')
        if self.options.orient == 'angle':
            marker_def.set('orient', str(self.options.angle))
        else:
            marker_def.set('orient', self.options.orient)
        if not self.options.scale:
            marker_def.set('markerUnits', 'userSpaceOnUse')
        # append marker content
        marker_def.append(marker)
        # make the path use it
        style = get_style(path)
        if len(style):
            for mprop in positions:
                style[mprop] = "url(#%s)" % marker_def_id
            path.set('style', simplestyle.formatStyle(style))
        else:
            inkex.errormsg(self.msg['Apply'])

    # --- Color Markers ---

    def get_colors_object(self, style):
        # object color
        obj_stroke = ('context-stroke' if self.options.context
                      else style.get('stroke', '#000000'))
        obj_fill = ('context-fill' if self.options.context
                    else style.get('fill', '#000000'))
        # object opacity
        stroke_opacity = (style.get('stroke-opacity', '1')
                          if self.options.assign_alpha else None)
        fill_opacity = (style.get('fill-opacity', '1')
                        if self.options.assign_alpha else None)
        # marker fill
        if self.options.marker_fill == "fill":
            fill = (obj_fill if not self.options.invert else obj_stroke)
        elif self.options.marker_fill == "stroke":
            fill = (obj_stroke if not self.options.invert else obj_fill)
        elif self.options.marker_fill == "none":
            fill = 'none'
        else:  # keep
            fill = None
        # marker stroke
        if self.options.marker_stroke == "stroke":
            stroke = (obj_stroke if not self.options.invert else obj_fill)
        elif self.options.marker_stroke == "fill":
            stroke = (obj_fill if not self.options.invert else obj_stroke)
        elif self.options.marker_stroke == "none":
            stroke = 'none'
        else:  # keep
            stroke = None
        # all set
        return {'fill': fill, 'stroke': stroke,
                'fill-opacity': fill_opacity,
                'stroke-opacity': stroke_opacity}

    def get_colors_custom(self):
        # marker opacity
        fill_opacity = None
        stroke_opacity = None
        # marker fill
        if self.options.fill_type == "assign":
            fill_red = ((self.options.fill_color >> 24) & 255)
            fill_green = ((self.options.fill_color >> 16) & 255)
            fill_blue = ((self.options.fill_color >> 8) & 255)
            fill = "rgb(%s,%s,%s)" % (fill_red, fill_green, fill_blue)
            fill_opacity = (((self.options.fill_color) & 255) / 255.0)
        elif self.options.fill_type == "cstroke":
            fill = 'context-stroke'
        elif self.options.fill_type == "cfill":
            fill = 'context-fill'
        elif self.options.fill_type == "none":
            fill = 'none'
        else:  # keep
            fill = None
        # marker stroke
        if self.options.stroke_type == "assign":
            stroke_red = ((self.options.stroke_color >> 24) & 255)
            stroke_green = ((self.options.stroke_color >> 16) & 255)
            stroke_blue = ((self.options.stroke_color >> 8) & 255)
            stroke = "rgb(%s,%s,%s)" % (stroke_red, stroke_green, stroke_blue)
            stroke_opacity = (((self.options.stroke_color) & 255) / 255.0)
        elif self.options.stroke_type == "cstroke":
            stroke = 'context-stroke'
        elif self.options.stroke_type == "cfill":
            stroke = 'context-fill'
        elif self.options.stroke_type == "none":
            stroke = 'none'
        else:  # keep
            stroke = None
        # all set
        return {'fill': fill, 'stroke': stroke,
                'fill-opacity': fill_opacity,
                'stroke-opacity': stroke_opacity}

    def get_marker_def(self, defs, style, mprop):
        marker_def = None
        if (mprop in style and
                style[mprop] != 'none' and
                style[mprop][:5] == 'url(#'):
            marker_id = style[mprop][5:-1]
            orig_marker_def = self.xpathSingle(
                '/svg:svg//svg:marker[@id="%s"]' % marker_id)
            if orig_marker_def is not None:
                if not self.options.modify:
                    marker_def = copy.deepcopy(orig_marker_def)
                else:
                    marker_def = orig_marker_def
            else:
                inkex.errormsg(_("unable to locate marker: %s") % marker_id)
            if marker_def is not None:
                new_id = self.uniqueId(marker_id, not self.options.modify)
                style[mprop] = "url(#%s)" % new_id
                marker_def.set('id', new_id)
                marker_def.set(inkex.addNS('stockid', 'inkscape'), new_id)
                defs.append(marker_def)
        return (style, marker_def)

    def color_markers(self, defs, style, markerstyle, positions):
        for marker_prop in positions:
            style, marker_def = self.get_marker_def(defs, style, marker_prop)
            if marker_def is not None:
                marker = marker_def.xpath('.//*[@style]', namespaces=inkex.NSS)
                for node in marker:
                    set_marker_style(node, markerstyle)
        return style

    # --- main effect ---

    def effect(self):
        # pylint: disable=too-many-branches
        # pylint: disable=global-statement

        global DEBUG
        DEBUG = self.options.debug

        defs = self.get_defs()
        positions = self.get_options(['marker-start',
                                      'marker-mid',
                                      'marker-end',
                                      'marker'])

        if not len(positions):
            inkex.errormsg(self.msg['Positions'])
        else:  # one or more marker positions active
            if self.options.tab == '"apply_edit"':
                # Edit and apply markers
                # TODO: use zSort for items selected to apply marker
                if len(self.options.ids) >= 1:
                    path = self.selected[self.options.ids[0]]
                    if self.options.apply_edit_tab == '"apply"':
                        if len(self.options.ids) == 2:
                            marker = self.selected[self.options.ids[1]]
                            self.apply_marker(marker, path, defs, positions)
                        else:  # no marker selected
                            inkex.errormsg(self.msg['Apply'])
                    elif self.options.apply_edit_tab == '"edit"':
                        self.edit_markers(path, defs, positions)
                else:  # nothing selected
                    inkex.errormsg(self.msg[self.options.getput])

            else:
                # Apply custom colors
                if self.options.tab == '"custom"':
                    markerstyle = self.get_colors_custom()
                    self.options.modify = self.options.modify_for_custom
                for node in self.selected.values():
                    style = get_style(node)
                    # Use object colors
                    if self.options.tab == '"object"':
                        markerstyle = self.get_colors_object(style)
                        self.options.modify = self.options.modify_for_object
                    # modify marker styles, return object style with new ref
                    style = self.color_markers(defs, style, markerstyle,
                                               positions)
                    # update object style with new marker ref
                    node.set('style', simplestyle.formatStyle(style))


if __name__ == '__main__':
    ME = CustomMarkers()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
